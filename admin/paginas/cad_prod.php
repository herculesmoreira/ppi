<?php
    if(isset($_GET['btnSalvar']))
    {
        $id = empty($_GET['id'])|| $_GET['id'] < 0 ? null : $_GET['id'];
        $nome = $_GET['nome'];
        $descricao = $_GET['descricao'];
        $preco = $_GET['preco'];
        $qtd = $_GET['quantidade'];
        $catNome = $_GET['catNome'];
        $categoria = models\Categoria::obterPeloNome($catNome)[0];

        if ($categoria == null)
            $categoria = new \models\Categoria(null, $catNome, true);

        $produto = new models\Produto($id, $nome, $descricao, $preco, $qtd, $categoria);
        $produto->salvar();
        $sucesso_salvar = true;
    }

    $categorias = models\Categoria::obterTodos();
    $prod = null;
    $op = $_GET['op'];
    if($op == 'edit')
        $prod = models\Produto::obterPeloId($_GET['produtoID']);

?>
<!-- cadastro produto -->
<div class="container container-conteudo">
    <?php if ($op == 'edit'){ ?>
        <h2>EDITAR PRODUTO</h2>
    <?php }else{ ?>
        <h2>CADASTRAR PRODUTO</h2>
    <?php } ?>
    <hr>

    <?php if($sucesso_salvar) {  ?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong>Sucesso!</strong> Os dados foram salvos.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    <?php } ?>
    <form method="get" action="">

        <div class="form-row">
            <div class="form-group col-md-6">
                <input type="hidden" name="p" value="<?php echo $_GET['p'] ?>">
                <input type="hidden" name="id" value="<?php if($op == 'edit') echo $prod->getId(); ?>">
                <label for="nome">Nome*</label>
                <input required name="nome" type="text" class="form-control" id="nome" placeholder="Nome" value="<?php if($op == 'edit') echo $prod->getNome(); ?>">
            </div>

            <div class="form-group col-md-6">
                <label for="desc">Descrição</label>
                <input name="descricao" type="text" class="form-control" id="desc" placeholder="Descrição" value="<?php if($op == 'edit') echo $prod->getDescricao(); ?>">
            </div>
        </div>

        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="preco">Preço*</label>
                <input required name="preco" type="number" step="0.01" min="0" max="999999999" class="form-control" id="preco" placeholder="Preço de venda" value="<?php if($op == 'edit') echo $prod->getPreco(); ?>">
            </div>

            <div class="form-group col-md-6">
                <label for="qtd">Quantidade*</label>
                <input required name="quantidade" type="number" class="form-control" id="qtd" placeholder="Quantidade em estoque" value="<?php if($op == 'edit') echo $prod->getQuantidade(); ?>">
            </div>
        </div>

        <div class="form-row">
            <div class="form-group col-md-6 d-flex align-items-center">
                <p>Campos com <b>*</b> são obrigatórios.</p>
            </div>

            <div class="form-group col-md-6">
                <label for="categoria">Categoria*</label>
                <input required class="form-control" type="text" name="catNome" id="categoria" list="cars" value="<?php if($op == 'edit') echo $prod->getCategoria()->getDescricao() ?>">
                <datalist id="cars">
                    <?php foreach($categorias as $item) { ?>
                    <option><?php echo $item->getDescricao() ?></option>
                    <?php } ?>
                </datalist>
            </div>


            <div class="form-group col-md-8">
            </div>

            <div class="form-group col-md-4 d-flex justify-content-end">
                <button type="submit" class="btn btn-outline-primary" name="btnSalvar">Salvar </button>
            </div>

        </div>

    </form>
</div>

