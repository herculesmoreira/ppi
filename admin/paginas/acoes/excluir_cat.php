<?php
$categoria = models\Categoria::obterPeloId($_GET['categoriaID']);

if($categoria != null){
    $categoria->excluir();
    $paginaVoltar = $_GET['paginaVoltar'];
    $paginaVoltar .= '&exclusao_status=1';
    header("Location: {$paginaVoltar}");
    exit();
}else{
    $paginaVoltar = $_GET['paginaVoltar'];
    $paginaVoltar .= '&exclusao_status=-1';
    header("Location: {$paginaVoltar}");
    exit();
}
?>
