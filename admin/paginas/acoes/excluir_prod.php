<?php
    $produto = models\Produto::obterPeloId($_GET['produtoID']);

    if($produto != null){
        $produto->excluir();
        $paginaVoltar = $_GET['paginaVoltar'];
        $paginaVoltar .= '&exclusao_status=1';
        header("Location: {$paginaVoltar}");
        exit();
    }else{
        $paginaVoltar = $_GET['paginaVoltar'];
        $paginaVoltar .= '&exclusao_status=-1';
        header("Location: {$paginaVoltar}");
        exit();
    }
?>
