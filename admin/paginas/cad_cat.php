<?php

    if(isset($_GET['btnSalvar']))
    {
        $id = empty($_GET['id']) ||$_GET['id'] < 0 ? null : $_GET['id'];
        $desc = $_GET['desc'];

        $categoria = new \models\Categoria($id, $desc, true);
        $categoria->salvar();
        $sucesso_salvar = true;
    }

    $categoria = null;
    if($_GET['op'] == 'edit')
        $categoria = \models\Categoria::obterPeloId($_GET['categoriaID']);
?>

<!-- cadastro produto -->
<div class="container container-conteudo">
    <?php if ($_GET['op'] == 'edit') { ?>
        <h2>EDITAR CATEGORIA</h2>
    <?php }else { ?>
        <h2>CADASTRAR CATEGORIA</h2>
    <?php } ?>
    <hr>

    <?php if($sucesso_salvar) {  ?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong>Sucesso!</strong> Os dados foram salvos.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    <?php } ?>
    <form method="get">

        <div class="form-row">
            <div class="form-group col-md-12">
                <label for="nome">Descricao</label>
                <input type="hidden" name="p" value="<?php echo $_GET['p']?>">
                <input type="hidden" name="id" value="<?php if ($_GET['op'] == 'edit') echo $categoria->getId() ?>">
                <input required type="text" name="desc" class="form-control" id="desc" placeholder="Nome" value="<?php if ($_GET['op'] == 'edit') echo $categoria->getDescricao() ?>">
            </div>

            <div class="form-group col-md-6 d-flex justify-content-end">

            </div>

            <div class="form-group col-md-6 d-flex justify-content-end">
                <button name="btnSalvar" type="submit" class="btn btn-outline-primary">Salvar </button>
            </div>

        </div>

    </form>
</div>

