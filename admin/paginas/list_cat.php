<?php
    $categorias = models\Categoria::obterTodos();

    if(isset($_GET['btnBuscar']))
        $categorias = models\Categoria::obterPeloNome($_GET['pesquisa']);

    if($categorias == null)
        $categorias = array();
?>

<div class="container container-conteudo">
    <h2>CONSULTAR CATEGORIAS</h2>
    <hr>
    <form>
        <div class="form-row">
            <div class="form-group col-md-4"></div>

            <div class="form-group col-md-4">

            </div>

            <div class="form-group col-md-4 d-flex justify-content-end">
                <div class="input-group">
                    <input type="hidden" name="p" value="<?php echo $_GET['p'] ?>">
                    <input type="text" class="form-control" id="desc" placeholder="Pesqusiar . . ." name="pesquisa" value="<?php echo $_GET['pesquisa']?>">
                    <div class="input-group-append">
                        <button type="submit" name="btnBuscar" class="btn btn-secondary d-flex align-items-center" type="button" id="pesquisar">
                            <ion-icon name="search"></ion-icon>
                        </button>
                    </div>
                </div>
            </div>

        </div>
    </form>

    <script type="text/javascript">
        function confirmar_exclusao_cat(link_exclusao, desc, id)
        {
            var respsota = confirm("Realmente deseja excluir categoria " + desc + " (código " + id + ")");
            var paginaVoltar = window.location.href;
            link_exclusao = link_exclusao + "&paginaVoltar=" + paginaVoltar;
            if(respsota)
                window.location.href = link_exclusao
        }
    </script>

    <?php if($_GET['exclusao_status'] == 1){ ?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong>Sucesso!</strong> O item foi excluído.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    <?php }
    if($_GET['exclusao_status'] == -1) {
        ?>
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <strong>Erro!</strong> O sistema falhou em excluir o item.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php
    }

    ?>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Descrição</th>
            <th scope="col">Status</th>
            <th scope="col">Ação</th>
        </tr>
        </thead>
        <tbody>
        <?php
            if(empty($categorias))
                echo "<tr><td colspan=\"4\" class=\"text-center\">Sem resultados para a busca '{$_GET['pesquisa']}'</td></tr>";
            foreach ($categorias as $item) {
                $link_exclusao = "{$uri}?p=acoes/excluir_cat&categoriaID={$item->getId()}";
        ?>
        <tr>
            <th scope="row"><?php echo $item->getId() ?></th>
            <td><?php echo $item->getDescricao() ?></td>
            <td class="text-success">Ativo</td>
            <td>
                <a href="<?php echo "{$uri}?p=cad_cat&op=edit&categoriaID={$item->getId()}" ?>"><button onclick="" class="btn btn-warning"><ion-icon name="create"></ion-icon></button></a>
                <button onclick="confirmar_exclusao_cat('<?php echo $link_exclusao  ?>', '<?php echo $item->getDescricao() ?>', <?php  echo $item->getId()?>)" class="btn btn-danger"><ion-icon  name="trash"></ion-icon></button>
            </td>
        </tr>
        <?php } ?>
        </tbody>
    </table>
</div>

