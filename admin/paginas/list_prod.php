<?php
use models\Produto;
use models\Categoria;

$categoria = null;
if(isset($_GET['categoriaID']) && $_GET['categoriaID'] >= 0)
    $categoria = Categoria::obterPeloId($_GET['categoriaID']);

$produtos = Produto::buscarComFiltro($_GET['pesquisa'], $categoria);
$categorias = Categoria::obterTodos();

$produtos = $produtos == null ? array() : $produtos;
$categorias = $categorias == null ? array() : $categorias;

?>

<div class="container container-conteudo">
    <h2>CONSULTAR PRODUTOS</h2>
    <hr>
    <form method="GET" action="">
        <div class="form-row">
            <div class="form-group col-md-4"></div>

            <div class="form-group col-md-4">
                <div class="input-group">
                    <div class="input-group-append">
                        <input type="hidden" name="p" value="<?php echo $_GET['p'] ?>">
                        <label class="input-group-text" for="selectCategoria">Filtrar Categoria </label>
                    </div>
                    <select class="form-control" id="selectCategoria" name="categoriaID">
                        <option <?php if(!isset($_GET['categoriaID'])) echo 'selected'  ?> value="-1">Todas</option>
                        <?php foreach ($categorias as $item) { ?>
                            <option <?php if($_GET['categoriaID'] == $item->getId()) echo 'selected' ?> value="<?php echo $item->getId() ?>"><?php echo $item->getDescricao() ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>

            <div class="form-group col-md-4 d-flex justify-content-end">
                <div class="input-group">
                    <input type="text" class="form-control" id="desc" placeholder="Pesqusiar . . ." name="pesquisa">
                    <div class="input-group-append">
                        <button type="submit" class="btn btn-secondary d-flex align-items-center" type="button" id="pesquisar">
                            <ion-icon name="search"></ion-icon>
                        </button>
                    </div>
                </div>
            </div>

        </div>
    </form>
    <script type="text/javascript">
        function confirmar_exclusao(link_exclusao, nome, codigo){
            var resposta = confirm("Realmente deseja excluir " + nome + " (Código = " + codigo + ") ?");
            var paginaAnterior = window.location.href;
            var link_final = link_exclusao + "&paginaVoltar=" + paginaAnterior;

            if(resposta) {
                window.location = link_final;
            }
        }
    </script>
    <?php if($_GET['exclusao_status'] == 1){ ?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong>Sucesso!</strong> O item foi excluído.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    <?php }
    if($_GET['exclusao_status'] == -1) {
    ?>
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <strong>Erro!</strong> O sistema falhou em excluir o item.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    <?php
    }

    ?>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Nome</th>
            <th scope="col">Descrição</th>
            <th scope="col">Preço</th>
            <th scope="col">Quantidade</th>
            <th scope="col">Ação</th>
        </tr>
        </thead>
        <tbody>
        <?php
            foreach ($produtos as $item) {
            $link_exclusao = "{$uri}?p=acoes/excluir_prod&produtoID={$item->getId()}";
        ?>
        <tr>
            <th scope="row"><?php echo $item->getId()  ?></th>
            <td><?php echo $item->getNome()  ?> <span class="badge badge-success"><?php echo $item->getCategoria()->getDescricao() ?></span></td>
            <td><?php echo $item->getDescricao()  ?></td>
            <td><?php echo $item->getPreco()  ?></td>
            <td><?php echo $item->getQuantidade()  ?></td>
            <td>
                <a href="<?php echo "{$uri}?p=cad_prod&op=edit&produtoID={$item->getId()}" ?>"><button class="btn btn-warning"><ion-icon name="create"></ion-icon></button></a>
                <button class="btn btn-danger" onclick="confirmar_exclusao('<?php echo $link_exclusao ?>', '<?php echo $item->getNome() ?>', <?php echo $item->getId() ?>)"><ion-icon  name="trash"></ion-icon></button>
            </td>
        </tr>
        <?php } ?>
        </tbody>
    </table>
</div>

