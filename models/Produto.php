<?php
/**
 * Created by PhpStorm.
 * User: Hércules
 * Date: 27/06/2019
 * Time: 12:13
 */

namespace models;

use models\dao\ProdutoDAO;

class Produto
{
    private $id;
    private $nome;
    private $descricao;
    private $preco;
    private $quantidade;
    private $categoria;
    private static $dao;

    public function __construct($id, $nome, $descricao, $preco, $quantidade, $categoria)
    {
        $this->id = $id;
        $this->nome = $nome;
        $this->descricao = $descricao;
        $this->preco = $preco;
        $this->quantidade = $quantidade;
        $this->categoria = $categoria;
    }

    public function getDao()
    {
        if(self::$dao == null)
            self::$dao = new ProdutoDAO();

        return self::$dao;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getNome()
    {
        return $this->nome;
    }

    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    public function getDescricao()
    {
        return $this->descricao;
    }

    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
    }

    public function getPreco()
    {
        return $this->preco;
    }

    public function setPreco($preco)
    {
        $this->preco = $preco;
    }

    public function getQuantidade()
    {
        return $this->quantidade;
    }

    public function setQuantidade($quantidade)
    {
        $this->quantidade = $quantidade;
    }

    public function getCategoria()
    {
        return $this->categoria;
    }

    public function setCategoria($categoria)
    {
        $this->categoria = $categoria;
    }


    public function salvar()
    {
        $this->categoria->salvar();
        if ($this->id != null && self::getDao()->obterPeloId($this->id) != null)
            self::getDao()->atualizar($this);
        else
            $this->id = self::getDao()->inserir($this);
    }

    public function excluir()
    {
        if (self::getDao()->obterPeloId($this->id) != null)
            self::getDao()->excluir($this);
    }

    public static function obterPeloId($id)
    {
        return self::getDao()->obterPeloId($id);
    }

    public static function obterPeloNome($nome)
    {
        return self::getDao()->obterPeloNome($nome);
    }

    public static function obterTodos()
    {
        return self::getDao()->obterTodos();
    }

    public static function buscarComFiltro($nome, $categoria)
    {
        return self::getDao()->buscarComFiltro($nome, $categoria);
    }


}
