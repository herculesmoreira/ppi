<?php
/**
 * Created by PhpStorm.
 * User: Hércules
 * Date: 27/06/2019
 * Time: 12:21
 */

namespace models;


use models\dao\CategoriaDAO;

class Categoria
{
    private $id;
    private $descricao;
    private $ativo;
    private static $dao = null;

    public function __construct($id, $descricao, $ativo = true)
    {
        $this->id = $id;
        $this->descricao = $descricao;
        $this->ativo = $ativo;
    }

    private static function getDao()
    {
        if (self::$dao == null)
            self::$dao = new CategoriaDAO();

    return self::$dao;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getDescricao()
    {
        return $this->descricao;
    }

    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
    }

    public function salvar()
    {
        if ($this->id != null && self::getDao()->obterPeloId($this->id) != null)
            self::getDao()->atualizar($this);
        else
            $this->id = self::getDao()->inserir($this);
    }

    public function excluir()
    {
        if( self::getDao()->obterPeloId($this->id) != null)
            self::getDao()->excluir($this);
    }

    public static function obterPeloId($id)
    {
        return self::getDao()->obterPeloId($id);
    }

    public static function obterPeloNome($nome)
    {
        return self::getDao()->obterPeloNome($nome);
    }

    public static function obterTodos()
    {
        return self::getDao()->obterTodos();
    }


}
