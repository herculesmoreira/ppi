<?php
/**
 * Created by PhpStorm.
 * User: Hércules
 * Date: 27/06/2019
 * Time: 12:13
 */

namespace models\dao;

use Exception;
use models\Categoria;
use models\Produto;
use PDO;

class ProdutoDAO
{
    private $conexao;
    private $tabela = 'produto';

    public function __construct()
    {
        $this->conexao = Conexao::obterInstancia();
    }

    public function inserir($produto)
    {
        if ($produto == null)
            throw new Exception('Parâmetro $produto é nulo');

        $consultaPreparada = $this->conexao->getPdo()->prepare("INSERT INTO {$this->tabela} (nome, descricao, preco, quantidade, categoria_id) VALUES (?, ?, ?, ?, ?)");
        $consultaPreparada->bindValue(1, $produto->getNome());
        $consultaPreparada->bindValue(2, $produto->getDescricao());
        $consultaPreparada->bindValue(3, $produto->getPreco());
        $consultaPreparada->bindValue(4, $produto->getQuantidade());
        $consultaPreparada->bindValue(5, $produto->getCategoria()->getId());
        $consultaPreparada->execute();
        return $this->conexao->getPdo()->lastInsertId();
    }

    public function obterPeloId($id)
    {
        if ($id === null)
            throw new Exception('Parâmetro $id é nulo');

        if($id < 0)
            throw new Exception('Parâmetro $id é negativo');

        $consultaPreparada = $this->conexao->getPdo()->prepare("SELECT p.*, c.descricao as cat_desc, c.ativo FROM {$this->tabela} AS p JOIN categoria AS c ON p.categoria_id = c.id WHERE p.id = ?");
        $consultaPreparada->bindValue(1, $id);
        $consultaPreparada->execute();
        $res = $consultaPreparada->fetch(PDO::FETCH_ASSOC);

        $categoria = new Categoria($res['categoria_id'], $res['cat_desc'], $res['ativo']);
        return new Produto($res['id'], $res['nome'], $res['descricao'], $res['preco'], $res['quantidade'], $categoria);
    }

    public function obterPeloNome($nome)
    {
        if(empty($nome))
            throw new Exceptio('Parâmetro $nome é nulo');

        $consultaPreparada = $this->conexao->getPdo()->prepare("SELECT p.*, c.descricao as cat_desc, c.ativo FROM {$this->tabela} AS p JOIN categoria AS c ON p.categoria_id = c.id WHERE p.nome LIKE ?");
        $consultaPreparada->bindValue(1, "{$nome}%");
        $consultaPreparada->execute();
        
        $res = $consultaPreparada->fetchAll();
        
        if(empty($res))
            return null;

        $objetosProd = array();
        foreach ($res as $item) {
            $categoria = new Categoria($item['categoria_id'], $item['cat_desc'], $item['ativo']);
            $objetosProd[] = new Produto(
                $item['id'],
                $item['nome'],
                $item['descricao'],
                $item['preco'],
                $item['quantidade'],
                $categoria
            );
        }

        return $objetosProd;
    }

    public function obterTodos()
    {
        $consultaPreparada = $this->conexao->getPdo()->prepare("SELECT p.*, c.descricao as cat_desc, c.ativo FROM {$this->tabela} AS p JOIN categoria AS c ON p.categoria_id = c.id");
        $consultaPreparada->execute();

        $res = $consultaPreparada->fetchAll();

        if(empty($res))
            return null;

        $objetosProd = array();
        foreach ($res as $item) {
            $categoria = new Categoria($item['categoria_id'], $item['cat_desc'], $item['ativo']);
            $objetosProd[] = new Produto(
                $item['id'],
                $item['nome'],
                $item['descricao'],
                $item['preco'],
                $item['quantidade'],
                $categoria
            );
        }

        return $objetosProd;
    }

    public function buscarComFiltro($nome, $categoria)
    {
        $sql = "SELECT p.*, c.descricao as cat_desc, c.ativo FROM {$this->tabela} AS p JOIN categoria AS c ON p.categoria_id = c.id WHERE p.nome LIKE ?";

        if($categoria != null)
            $sql .= ' AND categoria_id = ?';

        $consultaPreparada = $this->conexao->getPdo()->prepare($sql);
        $consultaPreparada->bindValue(1, "%{$nome}%");

        if($categoria != null)
            $consultaPreparada->bindValue(2, $categoria->getId());

        $consultaPreparada->execute();

        $res = $consultaPreparada->fetchAll();

        if(empty($res))
            return null;

        $objetosProd = array();
        foreach ($res as $item) {
            $categoria = new Categoria($item['categoria_id'], $item['cat_desc'], $item['ativo']);
            $objetosProd[] = new Produto(
                $item['id'],
                $item['nome'],
                $item['descricao'],
                $item['preco'],
                $item['quantidade'],
                $categoria
            );
        }

        return $objetosProd;
    }

    public function atualizar($produto){
        if ($produto == null)
            throw new Exception('Parâmetro $produto é nulo');

        if($produto->getId() == null || $produto->getId < 0)
            throw new Exception('Valor de $produto->getId() é nulo ou inválido');

        $sql = "UPDATE {$this->tabela} SET nome = ?, descricao = ?, preco = ?, quantidade = ?, categoria_id = ? WHERE id = ?";
        $consultaPreparada = $this->conexao->getPdo()->prepare($sql);
        $consultaPreparada->bindValue(1, $produto->getNome());
        $consultaPreparada->bindValue(2, $produto->getDescricao());
        $consultaPreparada->bindValue(3, $produto->getPreco());
        $consultaPreparada->bindValue(4, $produto->getQuantidade());
        $consultaPreparada->bindValue(5, $produto->getCategoria()->getId());
        $consultaPreparada->bindValue(6, $produto->getId());

        $consultaPreparada->execute();
    }

    public function excluir($produto)
    {
        if ($produto == null)
            throw new Exception('Parâmetro $produto é nulo');

        if($produto->getId() == null || $produto->getId < 0)
            throw new Exception('Valor de $produto->getId() é nulo ou inválido');

        $consultaPreparada = $this->conexao->getPdo()->prepare("DELETE FROM {$this->tabela} WHERE id = ?");
        $consultaPreparada->bindValue(1, $produto->getId());
        $consultaPreparada->execute();
    }
}
