<?php
/**
 * Created by PhpStorm.
 * User: Hércules
 * Date: 27/06/2019
 * Time: 12:35
 */

namespace models\dao;

use models\Categoria;
use models\dao\Conexao;
use PDO;
use Exception;

class CategoriaDAO
{
    private $conexao;
    private $tabela = 'categoria';

    public function __construct()
    {
        $this->conexao = Conexao::obterInstancia();
    }

    public function inserir($categoria)
    {
        if($categoria == null)
            throw new Exception('Parâmetro $categoria é nulo');

        if($categoria->getDescricao() == null)
            throw new Exception('Atributo $categoria->getDescricao() retornou nulo');

        $consultaPreparada = $this->conexao->getPdo()->prepare("INSERT INTO {$this->tabela} (descricao) VALUES (?)");
        $consultaPreparada->bindValue(1, $categoria->getDescricao());
        $consultaPreparada->execute();

        return $this->conexao->getPdo()->lastInsertId();
    }

    public function obterPeloId($id)
    {
        if($id === null)
            throw new Exception('Parâmetro $id é nulo');

        if($id < 0)
            throw new Exception('Parâmetro $id é negativo');

        $consultaPreparada = $this->conexao->getPdo()->prepare("SELECT * FROM {$this->tabela} WHERE id = ? AND ativo = 1");
        $consultaPreparada->bindValue(1, $id);
        $consultaPreparada->execute();
        $resultado = $consultaPreparada->fetch(PDO::FETCH_ASSOC);

        if(empty($resultado))
            return null;


        return new Categoria($resultado['id'], $resultado['descricao'], $resultado['ativo']);
    }

    public function obterPeloNome($nome)
    {
        $consultaPreparada = $this->conexao->getPdo()->prepare("SELECT * FROM {$this->tabela} WHERE descricao LIKE ? AND ativo = 1");
        $consultaPreparada->bindValue(1, "%{$nome}%");
        $consultaPreparada->execute();
        $resultado = $consultaPreparada->fetchAll();

        if(empty($resultado))
            return null;

        $objetosCat = array();
        foreach ( $resultado as $item)
        {
            $objetosCat[] = new Categoria($item['id'], $item['descricao'], $item['ativo']);
        }

        return $objetosCat;
    }

    public function obterTodos()
    {
        $consultaPreparada = $this->conexao->getPdo()->prepare("SELECT * FROM {$this->tabela} WHERE ativo = 1");
        $consultaPreparada->execute();
        $resultado = $consultaPreparada->fetchAll();

        if(empty($resultado))
            return null;

        $objetosCat = array();
        foreach ( $resultado as $item)
        {
            $objetosCat[] = new Categoria($item['id'], $item['descricao'], $item['ativo']);
        }

        return $objetosCat;
    }

    public function atualizar($categoria)
    {
        if($categoria == null)
            throw new Exception('Parâmetro $categoria é nulo');

        if($categoria->getId() === null || $categoria->getId() < 0)
            throw new Exception('Atributo $categoria->getId() retornou nulo ou é inválido');

        if($categoria->getDescricao() == null)
            throw new Exception('Atributo $categoria->getDescricao() retornou nulo');

        $consultaPreparada = $this->conexao->getPdo()->prepare("UPDATE {$this->tabela} SET descricao = ? WHERE id = ?");
        $consultaPreparada->bindValue(1, $categoria->getDescricao());
        $consultaPreparada->bindValue(2, $categoria->getId());
        $consultaPreparada->execute();
    }

    public function excluir($categoria)
    {
        if($categoria == null)
            throw new Exception('Parâmetro $categoria é nulo');

        if($categoria->getId() === null || $categoria->getId() < 0)
            throw new Exception('Atributo $categoria->getId() retornou nulo ou é inválido');

        if($categoria->getDescricao() == null)
            throw new Exception('Atributo $categoria->getDescricao() retornou nulo');

        $consultaPreparada = $this->conexao->getPdo()->prepare("UPDATE {$this->tabela} SET ativo = 0 WHERE id = ?");
        $consultaPreparada->bindValue(1, $categoria->getId());
        $consultaPreparada->execute();
    }
}