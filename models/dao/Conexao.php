<?php
/**
 * Created by PhpStorm.
 * User: Hércules
 * Date: 27/06/2019
 * Time: 12:14
 */

namespace models\dao;

use \PDO;

class Conexao
{
    private static $conexao = null;
    private $host    = 'localhost';
    private $usuario = 'root';
    private $senha   = '12345678';
    private $nome    = 'ppi';
    private $pdo     = null;

    private function __construct()
    {
        $dsn = "mysql:host={$this->host};dbname={$this->nome}";
        $this->pdo = new PDO($dsn, $this->usuario, $this->senha);
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    private static function instanciaValida()
    {
        return (self::$conexao != null);
    }

    public static function obterInstancia()
    {
        if( !self::instanciaValida() )
            self::$conexao = new Conexao();

        return self::$conexao;
    }

    public function getPdo()
    {
        return $this->pdo;
    }

}
