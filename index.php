<?php
/**
 * Created by PhpStorm.
 * User: Hércules
 * Date: 27/06/2019
 * Time: 12:12
 */

require_once('autoload.php');
use models\Produto;
use models\Categoria;

$categoria = null;
if(isset($_GET['categoriaID']) && $_GET['categoriaID'] >= 0)
    $categoria = Categoria::obterPeloId($_GET['categoriaID']);

$produtos = Produto::buscarComFiltro($_GET['pesquisa'], $categoria);
$categorias = Categoria::obterTodos();

$produtos = $produtos == null ? array() : $produtos;
$categorias = $categorias == null ? array() : $categorias;

?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="css/personalizado.css">
    <script src="https://unpkg.com/ionicons@4.5.10-0/dist/ionicons.js"></script>
    <title>OpressorStore - A loja que te oprime</title>
</head>
<body>
<div class="container-fluid" id="container-principal">
    <header class="container-fluid bg-dark">
        <div class="container">
            <div class="row py-4">
                <div class="col">
              <span class="text-danger">
                  <h4>OpressorStore</h4>
              </span>
                </div>

                <div class="col">
                </div>

                <div class="col d-flex justify-content-end align-items-center">
                    <button class="btn btn-outline-primary d-flex align-items-center"><ion-icon size="medium" name="cart"></ion-icon> &nbspCarrinho</button>
                </div>
            </div>
        </div>
    </header>

    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container">
            <!--<a class="navbar-brand" href="#">Navbar</a>-->
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#conteudoNavbarSuportado" aria-controls="conteudoNavbarSuportado" aria-expanded="false" aria-label="Alterna navegação">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="conteudoNavbarSuportado">
                <ul class="navbar-nav mr-auto">
                </ul>

                <form class="form-inline my-2 my-lg-0" method="GET" action="">
                    <div class="input-group">
                        <div class="input-group-append">
                            <label class="input-group-text" for="selectCategoria">Filtrar Categoria </label>
                        </div>
                        <select class="form-control" id="selectCategoria" name="categoriaID">
                            <option <?php if(!isset($_GET['categoriaID'])) echo 'selected'  ?> value="-1">Todas</option>
                            <?php foreach ($categorias as $item) { ?>
                                <option <?php if($_GET['categoriaID'] == $item->getId()) echo 'selected' ?> value="<?php echo $item->getId() ?>"><?php echo $item->getDescricao() ?></option>
                            <?php } ?>
                        </select>
                    </div>&nbsp
                    <div class="input-group">
                        <input class="form-control" type="search" placeholder="Pesquisar . . ." aria-label="Pesquisar" name="pesquisa">
                        <div class="input-group-append">
                            <button type="submit" class="btn btn-secondary d-flex align-items-center" type="button" id="pesquisar">
                                <ion-icon name="search"></ion-icon>
                            </button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </nav>

    <div class="container" id="container-conteudo">

        <div class="row">

            <?php foreach ($produtos as $item){ ?>
            <div class="col-sm-4">
                <div class="card">
                    <img class="card-img-top" src="img/teste.jpg" alt="Imagem de capa do card">
                    <div class="card-body">
                        <h5 class="card-title"><?php echo $item->getNome() ?></h5>
                        <p class="card-text"><?php echo $item->GetDescricao() ?></p>
                        <p>Preço: <?php echo $item->getPreco() ?></p>
                        <span class="badge badge-success"><?php echo $item->getCategoria()->getDescricao() ?></span>
                    </div>
                </div>
            </div>
            <?php } ?>
    </div>



</div>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>
